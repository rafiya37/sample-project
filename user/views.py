from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .models import UserModel
from .forms import UserForm

# Create your views here.

def home(request):
	"""This method to display all users"""

	user = UserModel.objects.all()
	context = {'user': user, 'form' :UserForm()}

	return render(request, 'home.html', context)

def addUser(request):
	"""This method to add the new user"""

	if request.method == "POST":
		form = UserForm(request.POST)
		if form.is_valid():
			user = form.save()
			user.save()
			return HttpResponseRedirect(reverse('home'))
		else:
			return HttpResponse({'errors': form.errors})

