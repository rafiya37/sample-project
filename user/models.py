from django.db import models

# Create your models here.



class UserModel(models.Model):
	"""This model to manage users"""
	
	username = models.CharField(max_length=40)
	message = models.CharField(blank=True, null=True, max_length=225) 

	def __str__(self):
		return self.username

