from django.forms import ModelForm
from .models import UserModel

class UserForm(ModelForm):
	"""This form handles the user"""

	class Meta:
		model = UserModel
		fields = ['username', 'message']