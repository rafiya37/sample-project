$(document).ready(function(){
   // do jQuery

   $.form_submit = function() {

   		var name = $('#id_username').val();
   		var message = $('#id_message').val();

   		if (name.trim() == "") {
   			alert("Please enter the name");
   			return false;
   		} 

   		if (message.trim() == "") {
   			alert("Please enter the message");
   			return false;   			
   		}
   		$.post("/adduser/", {'username': name,
   							 'message': message,
   							 'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()})
   		.then(function(data){
   			if (data.errors) {
   				alert("Please Enter the Username and Message");
   			} else {
   				document.location.href = '/';
   			}
   		})
   		return false;  
   }

})

